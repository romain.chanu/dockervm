# Ajout des utilisateurs dans le groupe docker
Par défaut le playbook va ajouter au groupe docker un utilisateur
au nom de la distribution s'il existe.  
Vous pouvez aussi ajouter d'autres utilisateurs en utilisant
la variable  **users** (list)


# Installer docker sur une VM  sans 'upgrade'
Si vous souhaitez installer rapidement **docker**
il vous suffit de lancer la commande suivante:

    ansible-playbook -u ubuntu --private-key ~/chemin_vers_votre_cle -i IP1,IP2,IPX deploy.yml


# Mettre à jour ma VM puis installer Docker
Une étape plus longue mais qui va mettre à jour votre machine

    ansible-playbook -u ubuntu --private-key ~/chemin_vers_votre_cle -i IP1,IP2,IPX -e upgrade_before_install=True deploy.yml
